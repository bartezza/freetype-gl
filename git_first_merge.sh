#!/bin/bash

# First merge of the two master branches (between origin and sync remotes)

# NOTE: execute this after setup_repo.sh

git checkout orig-master
git pull
git checkout master
git merge orig-master --allow-unrelated-histories
git push -u origin master

