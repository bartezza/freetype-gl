#!/bin/bash

# Synchronize the master branches between our repo (origin) and the original repo (sync)

git checkout orig-master
git pull
git checkout master
git merge orig-master

