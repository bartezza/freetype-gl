/* Freetype GL - A C OpenGL Freetype engine
 *
 * Distributed under the OSI-approved BSD 2-Clause License.  See accompanying
 * file `LICENSE` for more details.
 */
#ifndef __OPEN_GL_H__
#define __OPEN_GL_H__

#if defined(__APPLE__)
#   include "TargetConditionals.h"
#   if TARGET_OS_SIMULATOR || TARGET_OS_IPHONE
#     if defined(FREETYPE_GL_ES_VERSION_3_0)
#       include <OpenGLES/ES3/gl.h>
#     else
#       include <OpenGLES/ES2/gl.h>
#     endif
#   else
#     include <OpenGL/gl.h>
#   endif
#elif defined(_WIN32) || defined(_WIN64)
#  include <GL/glew.h>
#  include <GL/wglew.h>
#elif defined(ANDROID) // added for Android (bartezza)
#    ifdef USE_GLES_1
#      include <GLES/gl.h>
#      include <GLES/glext.h>
#    elif defined(USE_GLES_2)
#      include <GLES2/gl2.h>
#      include <GLES2/gl2ext.h>
#    elif defined(USE_GLES_3)
#      include <GLES3/gl3.h>
#      include <GLES3/gl3ext.h>
//#include "GLES3Compatibility.h"
#    endif
#else
#  include <GL/glew.h>
#  include <GL/gl.h>
#endif

#endif /* OPEN_GL_H */
