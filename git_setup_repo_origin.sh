#!/bin/bash

# Setup our repo by issuing a first commit

ORIGIN_REMOTE="git@bitbucket.org:bartezza/freetype-gl.git"

git remote add origin $ORIGIN_REMOTE
touch FORK.txt
git add FORK.txt
git commit -m "Initial commit"
git push --set-upstream origin master

