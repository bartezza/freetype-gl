#!/bin/bash

# Setup the original repo from which we are forking (sync)

SYNC_REMOTE="https://github.com/rougier/freetype-gl"

git remote add sync $SYNC_REMOTE
git remote -v
git fetch sync
git branch --set-upstream orig-master sync/master

